import os
import json
import glob
import math
from PIL import Image

class Extractor:
	def __init__(self):
		self.dynamic = [ [ 0 for  x in range(4096)] for y in range(4096)]
		self.grid = [ [ [0,0,0]  for  x in range(128)]  for y in range(128)]
		with open("./config.json") as file:    
			self.config = json.load(file)

	def simplifyElementsOfList(self, list):
		return [os.path.basename(item).replace(self.config["inputFormat"], "") for item in list]

	def loadStatus(self):
		self.statusList = []
		try:
			file = open(self.config["statusFile"])
			self.statusList = self.simplifyElementsOfList(file.read().splitlines())
		except IOError:
			open(self.config["statusFile"], 'a').close()

	def loadInputList(self):
		self.inputList = self.simplifyElementsOfList(glob.glob(self.config["inputDir"]+"\\*"+self.config["inputFormat"]))

	def skipElementsInList(self, b, a):
		return list(filter(lambda x: x not in b, a))

	def loadKeys(self):
		self.hairKey = (int(self.config["maskHairKey"]["r"]),int(self.config["maskHairKey"]["g"]),int(self.config["maskHairKey"]["b"]),int(self.config["maskHairKey"]["a"]))
		self.noHairKey = (int(self.config["maskNoHairKey"]["r"]),int(self.config["maskNoHairKey"]["g"]),int(self.config["maskNoHairKey"]["b"]),int(self.config["maskHairKey"]["a"]))

	def updateStatus(self, inputName):
		with open(self.config["statusFile"], "a") as file:
			file.write(inputName +"\n")

	def loadInput(self, inputName):
		image = Image.open(self.config["inputDir"]+"\\"+inputName+".jpg")
		mask  = Image.open(self.config["inputDir"]+"\\"+inputName+"_mask.png")
		return image, mask

	# Przyjmuje obrazek oraz maskę. Do zadanego katalogu zapisuje kwadratowe podobrazki ktre mieszczą się w masce
	def processInput(self, name, image, mask, key, outputDir):
		width,height = image.size

		# grid[gridY][gridX] = (x,y,s). (x,y) prawa dolna współrzędna kwadratu o boku s który daje się zbudować
		gridSize = 32
		gridWidth = math.ceil(width / gridSize)
		gridHeight = math.ceil(height / gridSize)
		


		# część dynamiczna. dynam[y][x] = s mówi że z punktu (x,y) daje się zbydować kwadrat o boku s.
		for x in range(width):
			self.dynamic[0][x]= ( key == mask.getpixel((x,0)))
		for y in range(height):
			self.dynamic[y][0]= ( key == mask.getpixel((0,y)))	
		
		for y in range(1, height-1):
			for x in range(1, width-1):
				self.dynamic[y][x] = 0
				if key != mask.getpixel((x,y)):
					continue
				a = min(self.dynamic[y-1][x], self.dynamic[y][x-1])
				self.dynamic[y][x] = a + 1
				if self.dynamic[y-a][x-a] == 0:
					self.dynamic[y][x] = a

		for gridY in range(0, gridHeight-1):
			for gridX in range(0, gridWidth-1):
				self.grid[gridY][gridX] = [0,0,0] 
				for y in range( gridSize * gridY,gridSize * (gridY+1)  ):
					for x in range(gridSize * gridX,gridSize * (gridX+1) ):

						if self.dynamic[y][x] > self.grid[gridY][gridX][2]:
							self.grid[gridY][gridX][2] = self.dynamic[y][x]
							self.grid[gridY][gridX][0] = x
							self.grid[gridY][gridX][1] = y


		# Wybieranie obrazka z każdego grida
		counter = 0			
		for gridY in range(0, gridHeight-1):
			for gridX in range(0, gridWidth-1):
				if self.grid[gridY][gridX][2] >= gridSize:
					subimage = image.crop((self.grid[gridY][gridX][0]-gridSize+1, self.grid[gridY][gridX][1]-gridSize+1, 
										self.grid[gridY][gridX][0] +1, self.grid[gridY][gridX][1] +1))
					subimage.save(outputDir+"\\"+name+"_"+str(counter)+".jpg")
					counter += 1
					
	def start(self):
		self.loadStatus();
		print("Skipping ", len(self.statusList), " files.")
		self.loadInputList();
		self.reducedInputList = self.skipElementsInList(self.statusList, self.inputList )
		self.loadKeys()

		current = len(self.statusList)+1
		total = len(self.inputList)
		for input in self.reducedInputList:
			print("Processing file", current, "/",total)

			image, mask = self.loadInput(input)

			self.processInput(input, image, mask, self.hairKey, 	self.config["outputHairDir"])
			self.processInput(input, image, mask, self.noHairKey, 	self.config["outputNoHairDir"])

			self.updateStatus(input)
			current += 1
		print("Done.")

### Execution
extractor = Extractor()
extractor.start()