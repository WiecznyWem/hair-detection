#%%
import glob
import numpy as np
import cv2
%matplotlib inline
import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import normalize
#%% Load images
hair_input_list = glob.glob("E:\\Desktop\\hair-data\\outputHair\\*.jpg")

data = np.zeros(shape=(len(hair_input_list),32*32),dtype=np.float32 )
for i in range(len(hair_input_list)):
    tmp = cv2.cvtColor(cv2.imread(hair_input_list[i],cv2.IMREAD_COLOR), cv2.COLOR_BGR2GRAY)
    
    data[i] = tmp.flatten()/255.0#normalize(tmp, copy=False).flatten()

#%%
print(data.shape)
#%%
model = IsolationForest()
model.fit(data)
#%%
the_test = cv2.cvtColor(cv2.imread("E:\\Desktop\\hair-data\\crazy.jpg", cv2.IMREAD_COLOR), cv2.COLOR_BGR2GRAY)
w = the_test.shape[0]//32
h = the_test.shape[1]//32
val = np.zeros((w,h))
for y in range(w):
    for x in range(h):
        img = the_test[32*y:32*y+32, 32*x:32*x+32]
        img = img.flatten().reshape(1,-1)/255.0
        print(img[0][0])
        val[-y][x] = model.predict(img)
    print(y,"/",w)
plt.imshow(val,extent=[0,the_test.shape[1],0,the_test.shape[0]])
plt.colorbar()
plt.imshow(the_test,alpha=0.6)
plt.show()