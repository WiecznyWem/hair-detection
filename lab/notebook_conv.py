#%%
import glob
import numpy as np
import cv2
from sklearn.model_selection import train_test_split

#%% Load images
hair_input_list = glob.glob("E:\\Desktop\\hair\\outputHair\\*.jpg")
no_hair_input_list = glob.glob("E:\\Desktop\\hair\\outputNoHair\\*.jpg")
number_to_load = min(len(hair_input_list), len(no_hair_input_list))

data = np.zeros(shape=(2*number_to_load,32,32,3),dtype=np.uint8)
target = np.empty(shape=(2*number_to_load,1),dtype=np.uint8)

for i in range(number_to_load):
    data[i] = cv2.cvtColor(cv2.imread(hair_input_list[i],cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
    target[i] = 1

for i in range(number_to_load):
    data[number_to_load+i] =  cv2.cvtColor(cv2.imread(no_hair_input_list[i],cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
    target[number_to_load+i] = 0
#%% Split data
x_train, x_test, y_train, y_test = train_test_split(data, target, 
                                        test_size=0.33, random_state=43)
#%% Machine learning part
import tensorflow as tf
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.layers import Conv2D, MaxPooling2D
from keras.losses import binary_crossentropy
from keras.optimizers import Adadelta, Adam, Adagrad
#%%
model = Sequential()
model.add(Conv2D(48, kernel_size=(5, 5), input_shape=(32,32,3)))
model.add(Activation('relu'))
model.add(Conv2D(48, kernel_size=(3, 3)))
model.add(Activation('relu'))

model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(96, kernel_size=(3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Conv2D(96, kernel_size=(3, 3)))
model.add(Activation('relu'))

model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(192, kernel_size=(3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Conv2D(192, kernel_size=(3, 3)))
model.add(Activation('relu'))

model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())

model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(256))
model.add(Activation('relu'))
model.add(Dropout(0.5))

model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy', optimizer=Adadelta(), metrics=['accuracy'])
print(model.summary())

#%%
print(type(x_train))
print (x_train.shape)
print(y_train.shape)

#%%
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
model.fit(x_train, y_train, batch_size=125, epochs=100, verbose=1, validation_data=(x_test, y_test))

#%%
from sklearn.metrics import accuracy_score
print(accuracy_score(np.around(model.predict(x_train)), y_train))
print(accuracy_score(np.around(model.predict(x_test)), y_test))

#%%
#%%
%matplotlib inline
import matplotlib.pyplot as plt
imgplot = plt.imshow(x_test[512])
#%% The test
the_test = cv2.cvtColor(cv2.imread("E:\\Desktop\\hair\\finite_.jpg",cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
plt.imshow(the_test)
#%%
x = 100
y= 400
img = the_test[y:y+32, x:x+32]
plt.imshow(img)
print(model.predict(np.array([img])))

