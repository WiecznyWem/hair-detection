#%%
import glob
import numpy as np
import cv2
%matplotlib inline
import matplotlib.pyplot as plt
import os
import tensorflow as tf
os.environ['KERAS_BACKEND'] = 'tensorflow'
from keras.layers import Input, Dense
from keras.models import Model
from keras.layers import Flatten
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D
from keras.utils.np_utils import to_categorical
from keras import optimizers
#%% Load images
hair_input_list = glob.glob("E:\\Desktop\\hair-data\\outputHair\\*.jpg")

data = np.zeros(shape=(len(hair_input_list),32,32,3),dtype=np.float32 )
for i in range(len(hair_input_list)):
    tmp = cv2.cvtColor(cv2.imread(hair_input_list[i],cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
    
    data[i] = tmp/255.0

#%%
input_img = Input(shape=(32, 32, 3)) 

x = Conv2D(64, (8,8), activation='relu', padding='same', data_format="channels_last")(input_img)
x = MaxPooling2D((2, 2), padding='same', data_format="channels_last")(x)
x = Conv2D(32, (3, 3), activation='relu', padding='same', data_format="channels_last")(x)
x = MaxPooling2D((2, 2), padding='same', data_format="channels_last")(x)
x = Conv2D(16, (3, 3), activation='relu', padding='same', data_format="channels_last")(x)
encoded = MaxPooling2D((2, 2), padding='same')(x)

x = Conv2D(16, (3, 3), activation='relu', padding='same', data_format="channels_last")(encoded)
x = UpSampling2D((2, 2))(x)
x = Conv2D(32, (2, 2), activation='relu', padding='same', data_format="channels_last")(x)
x = UpSampling2D((2, 2))(x)
x = Conv2D(64, (8, 8), activation='relu', padding='same', data_format="channels_last")(x)
x = UpSampling2D((2, 2))(x)
decoded = Conv2D(3, (8, 8), activation='relu', padding='same', data_format="channels_last")(x)

autoencoder = Model(input_img, decoded)
encoder = Model(input_img, encoded)

autoencoder.compile(optimizer='rmsprop', loss='mean_squared_error')
#autoencoder.compile(optimizer=optimizers.Adam(lr=0.005, beta_1=0.95, beta_2=0.991, epsilon=1e-09, decay=0.1), loss='categorical_crossentropy')
print(autoencoder.summary())

#%%
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
h = autoencoder.fit(data, data,
                epochs=5,
                batch_size=128,
                verbose=2,
                shuffle=True,
                validation_data=(data, data))
#%%

#%%
n = 10 
plt.figure(figsize=(20, 4))
for i in range(n):
    ax = plt.subplot(2, n, i + 1)
    plt.imshow(data[i])
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    ax = plt.subplot(2, n, i + 1 + n)
    out = autoencoder.predict(data[i].reshape(1,32,32,3)  )
    plt.imshow(out[0])
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
plt.show()

#%%
plt.plot(h.history['loss'])
plt.plot(h.history['val_loss'])
plt.title('loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()


#%%
print(encoder.predict(data[0].reshape(1,32,32,3)).shape)

#%%
n = 10 
plt.figure(figsize=(20, 4))
for i in range(n):
    ax = plt.subplot(2, n, i + 1)
    plt.imshow(data[i])
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    ax = plt.subplot(2, n, i + 1 + n)
    out = autoencoder.predict(data[i].reshape(1,32,32,3)  )
    plt.imshow(out[0])
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
plt.show()